package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    HashMap<String, Double> map = new HashMap<>();
//hash map
    public Quoter() {
        map.put(String.valueOf(1), 10.0);
        map.put(String.valueOf(2), 45.0);
        map.put(String.valueOf(3), 20.0);
        map.put(String.valueOf(4), 35.0);
        map.put(String.valueOf(5), 50.0);
    }

    public double getBookPrice(String isbn) {
        if (Integer.parseInt(isbn) >= 1 && Integer.parseInt(isbn) <=5){
            return map.get(isbn);
        }
        return 0;
    }
}
